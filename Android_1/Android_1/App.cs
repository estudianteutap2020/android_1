﻿using Android_1.Controlador;
using Android_1.Vista;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
namespace Android_1
{
    public class App : Application
    {
        public App()
        {
            MainPage = new NavigationPage(new Login());
        }
    }
}
