﻿using Android_1.Controlador;
using Android_1.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
namespace Android_1.Vista
{
    public class Detalle_Notas : ContentPage
    {
        RelativeLayout Principal;
        Label Detalle, Primer_C, Segundo_C, Tercer_C, Resultado_F, Nota;
        Button Btn_1, Btn_2, Btn_3;
        BoxView Barra_Navegacion;
        Image Icono_Atras;
        TapGestureRecognizer Gesto_Atras;
        Estudiante MiNota1;

        public Detalle_Notas(Estudiante MiNota)
        {
            MiNota1 = MiNota;
            NavigationPage.SetHasNavigationBar(this, false);
            CrearVista();
            AgregarVista();
            AgregarEvento();

        }

        void CrearVista()
        {
            Gesto_Atras = new TapGestureRecognizer();

            Detalle = new Label
            {
                Text = "Detalle Notas",
                FontSize = 23,
                TextColor = Colores.Textos1,
                HorizontalTextAlignment = TextAlignment.Center
            };

            Icono_Atras = new Image
            {
                Source = Colores.Icono_Atras
            };

            Barra_Navegacion = new BoxView
            {
                BackgroundColor = Colores.Navegacion
            };

            Btn_1 = new Button
            {
                BackgroundColor = Colores.Textos,
                Text = MiNota1.Nota1.ToString(),
                CornerRadius = 32,
                FontSize = 50,
                TextColor = Colores.Navegacion1,
                IsEnabled = true
            };

            Btn_2 = new Button
            {
                BackgroundColor = Colores.Textos,
                Text = MiNota1.Nota2.ToString(),
                CornerRadius = 32,
                FontSize = 50,
                TextColor = Colores.Navegacion1,
                IsEnabled = true
            };

            Btn_3 = new Button
            {
                BackgroundColor = Colores.Textos,
                Text = MiNota1.Nota3.ToString(),
                CornerRadius = 32,
                FontSize = 50,
                TextColor = Colores.Navegacion1,
                IsEnabled = true
            };

            Resultado_F = new Label
            {
                TextColor = Colores.Textos1,
                Text = "RESULTADO FINAL",
                FontSize = 16,
                Margin = new Thickness(0, 0, 0, 30),
            };

            Nota = new Label
            {
                TextColor = Colores.Resultado,
                Text = MiNota1.CalculoNota.ToString(),
                FontSize = 40,
                Margin = new Thickness(0, 0, 0, 30)
            };

            Primer_C = new Label
            {
                TextColor = Colores.Textos1,
                Text = "Primer Corte",
                FontSize = 16,
                Margin = new Thickness(0, 0, 0, 30),
            };

            Segundo_C = new Label
            {
                TextColor = Colores.Textos1,
                Text = "Segundo Corte",
                FontSize = 16,
                Margin = new Thickness(0, 0, 0, 30),
            };

            Tercer_C = new Label
            {
                TextColor = Colores.Textos1,
                Text = "Tercer Corte",
                FontSize = 16,
                Margin = new Thickness(0, 0, 0, 30),
            };

            Principal = new RelativeLayout { BackgroundColor = Colores.Navegacion1 };
        }

        void AgregarVista()
        {
            Icono_Atras.GestureRecognizers.Add(Gesto_Atras);

            Principal.Children.Add(Barra_Navegacion,
            Constraint.RelativeToParent((p) => { return 0; }),                 //X : Posición horizontal
            Constraint.RelativeToParent((p) => { return 0; }),                 //Y : Posición vertical
            Constraint.RelativeToParent((p) => { return p.Width; }),           //W : Ancho control
            Constraint.RelativeToParent((p) => { return p.Height * 0.083; })); //H : Alto control

            Principal.Children.Add(Detalle,
            Constraint.RelativeToParent((p) => { return 0; }),                  //X : Posición horizontal
            Constraint.RelativeToParent((p) => { return p.Height * 0.022; }),   //Y : Posición vertical
            Constraint.RelativeToParent((p) => { return p.Width; }));           //W : Ancho control

            Principal.Children.Add(Icono_Atras,
            Constraint.RelativeToParent((p) => { return 0; }),                  
            Constraint.RelativeToParent((p) => { return 0; }),                  
            Constraint.RelativeToParent((p) => { return p.Width * 0.149; }),   
            Constraint.RelativeToParent((p) => { return p.Height * 0.083; })); 

            Principal.Children.Add(Primer_C,
            Constraint.RelativeToParent((p) => { return p.Width * 0.130; }),  //X : Posición horizontal
            Constraint.RelativeToParent((p) => { return p.Height * 0.170; })); //Y : Posición vertical

            Principal.Children.Add(Segundo_C,
            Constraint.RelativeToParent((p) => { return p.Width * 0.610; }),  
            Constraint.RelativeToParent((p) => { return p.Height * 0.170; })); 

            Principal.Children.Add(Btn_1,
            Constraint.RelativeToParent((p) => { return p.Width * 0.100; }),  
            Constraint.RelativeToParent((p) => { return p.Height * 0.200; }), 
            Constraint.RelativeToParent((p) => { return p.Width * 0.300; }),  
            Constraint.RelativeToParent((p) => { return p.Height * 0.150; }));

            Principal.Children.Add(Btn_2,
            Constraint.RelativeToParent((p) => { return p.Width * 0.600; }),  
            Constraint.RelativeToParent((p) => { return p.Height * 0.200; }), 
            Constraint.RelativeToParent((p) => { return p.Width * 0.300; }),  
            Constraint.RelativeToParent((p) => { return p.Height * 0.150; }));

            Principal.Children.Add(Tercer_C,
            Constraint.RelativeToParent((p) => { return p.Width * 0.375; }),    //X : Posición horizontal
            Constraint.RelativeToParent((p) => { return p.Height * 0.470; }));  //Y : Posición vertical

            Principal.Children.Add(Btn_3,
            Constraint.RelativeToParent((p) => { return p.Width * 0.350; }),  
            Constraint.RelativeToParent((p) => { return p.Height * 0.500; }), 
            Constraint.RelativeToParent((p) => { return p.Width * 0.300; }),  
            Constraint.RelativeToParent((p) => { return p.Height * 0.150; }));

            Principal.Children.Add(Resultado_F,
            Constraint.RelativeToParent((p) => { return p.Width * 0.335; }),   
            Constraint.RelativeToParent((p) => { return p.Height * 0.700; }));  
                                                                               
            Principal.Children.Add(Nota,
            Constraint.RelativeToParent((p) => { return p.Width * 0.435; }),  
            Constraint.RelativeToParent((p) => { return p.Height * 0.750; })); 

            Content = Principal;
        }

        void AgregarEvento()
        {
            Gesto_Atras.Tapped += Gesto_Atras_Tapped;
        }

        private async void Gesto_Atras_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}