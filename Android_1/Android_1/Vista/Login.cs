﻿using Android_1.Controlador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
namespace Android_1.Vista
{
    public class Login : ContentPage
    {
        RelativeLayout Principal;
        Label Bienvenidos;
        Entry Documento, Codigo_E, Contraseña;
        Button Acceder, O_Contraseña;
        Image Logo;

        public Login()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            CrearVista();
            AgregarVista();
            AgregarEvento();
            this.BackgroundColor = Colores.Navegacion;
        }

        void CrearVista()
        {
            Principal = new RelativeLayout
            {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Padding = new Thickness(32, 0, 32, 0)
            };

            Logo = new Image
            {
                Source = Colores.Logo
            };

            Bienvenidos = new Label
            {
                TextColor = Colores.Titulos,
                Text = "Bienvenidos",
                FontSize = 30,
                Margin = new Thickness(0, 0, 0, 30),

            };

            Documento = new Entry
            {
                Placeholder = "Número de Documento",
                PlaceholderColor = Colores.Textos1,
                //HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            Codigo_E = new Entry
            {
                Placeholder = "Código estudiante",
                PlaceholderColor = Colores.Textos1,
                //HorizontalOptions = LayoutOptions.CenterAndExpand
            };

            Contraseña = new Entry
            {
                Placeholder = "Contraseña",
                PlaceholderColor = Colores.Textos1,
                //HorizontalOptions = LayoutOptions.CenterAndExpand,
                IsPassword = true,
            };

            Acceder = new Button
            {
                BackgroundColor = Colores.Navegacion1,
                Text = "Ingresar",
                CornerRadius = 25,
                TextColor = Colores.Titulos
            };

            O_Contraseña = new Button
            {
                BackgroundColor = Colores.Navegacion,
                Text = "¿Olvidaste tú contraseña?",
                CornerRadius = 25,
                TextColor = Colores.Textos,
            };
        }

        void AgregarVista()
        {
            Principal.Children.Add(Logo,
                Constraint.RelativeToParent((p) => { return p.Width * 0.100; }),  //X: Posición horizontal
                Constraint.RelativeToParent((p) => { return p.Height * 0.050; }),  //Y: Posición vertical
                Constraint.RelativeToParent((p) => { return p.Width * 0.793; })); //W: Ancho de control
                //Constraint.RelativeToParent((p) => { return p.Height * 0.773; })); //H: Alto de control

            Principal.Children.Add(Bienvenidos,
                Constraint.RelativeToParent((p) => { return p.Width * 0.275; }),  
                Constraint.RelativeToParent((p) => { return p.Height * 0.225; }),  
                Constraint.RelativeToParent((p) => { return p.Width * 0.793; })); 
                //Constraint.RelativeToParent((p) => { return p.Height * 0.773; })); 

            Principal.Children.Add(Documento,
                Constraint.RelativeToParent((p) => { return p.Width * 0.104; }), 
                Constraint.RelativeToParent((p) => { return p.Height * 0.350; }), 
                Constraint.RelativeToParent((p) => { return p.Width * 0.800; }),  
                Constraint.RelativeToParent((p) => { return p.Height * 0.080; }));

            Principal.Children.Add(Codigo_E,
                Constraint.RelativeToParent((p) => { return p.Width * 0.104; }),  
                Constraint.RelativeToParent((p) => { return p.Height * 0.450; }), 
                Constraint.RelativeToParent((p) => { return p.Width * 0.800; }),  
                Constraint.RelativeToParent((p) => { return p.Height * 0.080; }));

            Principal.Children.Add(Contraseña,
                Constraint.RelativeToParent((p) => { return p.Width * 0.104; }),  
                Constraint.RelativeToParent((p) => { return p.Height * 0.550; }), 
                Constraint.RelativeToParent((p) => { return p.Width * 0.800; }),  
                Constraint.RelativeToParent((p) => { return p.Height * 0.080; }));

            Principal.Children.Add(Acceder,
                Constraint.RelativeToParent((p) => { return p.Width * 0.104; }),  
                Constraint.RelativeToParent((p) => { return p.Height * 0.750; }), 
                Constraint.RelativeToParent((p) => { return p.Height * 0.385; }), 
                Constraint.RelativeToParent((p) => { return p.Height * 0.080; }));

            Principal.Children.Add(O_Contraseña,
                Constraint.RelativeToParent((p) => { return p.Width * 0.104; }),  
                Constraint.RelativeToParent((p) => { return p.Height * 0.900; }), 
                Constraint.RelativeToParent((p) => { return p.Width * 0.830; }),  
                Constraint.RelativeToParent((p) => { return p.Height * 0.080; }));

            Content = Principal;
        }

        void AgregarEvento()
        {
            Acceder.Clicked += Acceder_Clicked;
            O_Contraseña.Clicked += O_Contraseña_Clicked;
        }

        private async void Acceder_Clicked(object sender, EventArgs e)
        {
            Console.WriteLine(e);
            if (string.IsNullOrEmpty(Documento.Text))
            {
                await DisplayAlert("ERROR", "Por favor Ingrese su nùmero de documeto", "ACEPTAR");
                return;
            }

            if (string.IsNullOrEmpty(Codigo_E.Text))
            {
                await DisplayAlert("ERROR", "Ingrese código de estudiante", "ACEPTAR");
                return;
            }

            if (string.IsNullOrEmpty(Contraseña.Text))
            {
                await DisplayAlert("ERROR", "Ingrese su contraseña", "ACEPTAR");
                return;
            }

            if (Codigo_E.Text.ToCharArray().All(Char.IsLetter))
            {
                await this.DisplayAlert("Alerta", "El código solo debe contener números", "ACEPTAR");
                return;
            }

            if (Documento.Text.Length < 8)
            {
                await this.DisplayAlert("Alerta", "Su número de documento debe tener minímo 8 digitos", "ACEPTAR");
                return;
            }

            if (Contraseña.Text.Length < 6)
            {
                await this.DisplayAlert("Alerta", "Su contraseña debe tener minímo 8 caracteres", "ACEPTAR");
                return;
            }
            else
            {
                await Navigation.PushAsync(new MasterPage());
                Navigation.RemovePage(this);
                //throw new NotImplementedException();
            }
        }

        private async void O_Contraseña_Clicked(object sender, EventArgs e)
        {
            Console.WriteLine(e);
            await Navigation.PushAsync(new Recuperar_Contraseña());
        }
    }
}
