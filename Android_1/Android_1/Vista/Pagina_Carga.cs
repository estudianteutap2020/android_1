﻿using Android_1.Controlador;
using Android_1.Modelo;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
namespace Android_1.Vista
{
    public class Pagina_Cargar : ContentPage
    {
        RelativeLayout Principal;
        Label Pagina;
        ListView listaDatosPersonales, listaDatosPersonales1, listaDatosPersonales2;
        List<Estudiante> DatosPersonales, DatosPersonales1, DatosPersonales2;
        BoxView Barra_Navegacion;
        Image Icono_Salir;
        TapGestureRecognizer Gesto_Salir;
        ActivityIndicator Carga1;

        public Pagina_Cargar()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            CrearVistas();
            AgregarVista();
            AgregarEventos();
            this.BackgroundColor = Colores.Textos1;
        }

        void CrearVistas()
        {
            Gesto_Salir = new TapGestureRecognizer();

            Carga1 = new ActivityIndicator
            {
                IsRunning = true,
                Color = Colores.Textos
            };

            Pagina = new Label
            {
                Text = "Pagina Principal",
                FontSize = 20,
                TextColor = Colores.Textos,
                HorizontalTextAlignment = TextAlignment.Center
            };

            Icono_Salir = new Image
            {
                Source = Colores.Icono_Salir
            };

            Barra_Navegacion = new BoxView
            {
                BackgroundColor = Colores.Navegacion
            };

            DatosPersonales = new List<Estudiante>();
            
            ListadoDeEstudiantes();
           
            listaDatosPersonales = new ListView
            {
                ItemsSource = DatosPersonales,
                ItemTemplate = new DataTemplate(typeof(CeldaEstudiantes)),
                RowHeight = 80,
                SeparatorColor = Colores.Textos
            };

            listaDatosPersonales1 = new ListView
            {
                ItemsSource = DatosPersonales1,
                ItemTemplate = new DataTemplate(typeof(CeldaEstudiantes)),
                RowHeight = 80,
                SeparatorColor = Colores.Textos
            };

            listaDatosPersonales2 = new ListView
            {
                ItemsSource = DatosPersonales2,
                ItemTemplate = new DataTemplate(typeof(CeldaEstudiantes)),
                RowHeight = 80,
                SeparatorColor = Colores.Textos
            };

            Principal = new RelativeLayout { BackgroundColor = Colores.Textos1 };
        }

        void AgregarVista()
        {
            Icono_Salir.GestureRecognizers.Add(Gesto_Salir);

            Principal.Children.Add(Barra_Navegacion,
            Constraint.RelativeToParent((p) => { return 0; }),                 //X : Posición horizontal
            Constraint.RelativeToParent((p) => { return 0; }),                 //Y : Posición vertical
            Constraint.RelativeToParent((p) => { return p.Width; }),           //W : Ancho de control
            Constraint.RelativeToParent((p) => { return p.Height * 0.083; })); //H : Alto de control

            Principal.Children.Add(Pagina,
            Constraint.RelativeToParent((p) => { return 0; }),                
            Constraint.RelativeToParent((p) => { return p.Height * 0.022; }), 
            Constraint.RelativeToParent((p) => { return p.Width; }));         
                                                                            
            Principal.Children.Add(Icono_Salir,
            Constraint.RelativeToParent((p) => { return p.Width * 0.850; }),  
            Constraint.RelativeToParent((p) => { return 0; }),                
            Constraint.RelativeToParent((p) => { return p.Width * 0.149; }),  
            Constraint.RelativeToParent((p) => { return p.Height * 0.083; }));

            Principal.Children.Add(listaDatosPersonales,
            Constraint.RelativeToParent((p) => { return 0; }),                
            Constraint.RelativeToParent((p) => { return p.Height * 0.100; }));
                                                                           
            Principal.Children.Add(Carga1,
            Constraint.RelativeToParent((p) => { return p.Width * 0.440; }),  
            Constraint.RelativeToParent((p) => { return p.Height * 0.430; }));
                                                                              
            Content = Principal;
        }

        void AgregarEventos()
        {

        }

        void ListadoDeEstudiantes()
        {
            DatosPersonales.Add(new Estudiante
            {
                Nombre = "Roney",
                Apellido = "Rodriguez",
                Docente = "Docente: Roney Rodriguez",
                Materia = "Electiva II",

            });

            DatosPersonales1.Add(new Estudiante
            {
                Nombre = "Roney",
                Apellido = "Rodriguez",
                Docente = "Docente: Roney Rodriguez",
                Materia = "POO II",

            });

            DatosPersonales2.Add(new Estudiante
            {
                Nombre = "Roney",
                Apellido = "Rodriguez",
                Docente = "Docente: Roney Rodriguez",
                Materia = "Web II",

            });

        }

        void ListadoDeEstudiantes1()
        {
        }

        void ListadoDeEstudiantes2()
        {

        }
    }
}
