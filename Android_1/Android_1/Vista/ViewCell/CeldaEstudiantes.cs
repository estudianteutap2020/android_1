﻿using Android_1.Controlador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
namespace Android_1.Vista
{
    public class CeldaEstudiantes : ViewCell
    {
        StackLayout Celda_Principal, Vista_Label;
        Label LabelNombreEstudiante, LabelMateria, LabelDocente, LabelNota;

        public CeldaEstudiantes()
        {
            Celda_Principal = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Margin = new Thickness(12, 0, 12, 0)
            };

            Vista_Label = new StackLayout
            {
                HorizontalOptions = LayoutOptions.StartAndExpand
            };

            LabelNombreEstudiante = new Label
            {

            };

            LabelMateria = new Label
            {

            };

            LabelDocente = new Label
            {

            };

            LabelNota = new Label
            {
                FontSize = 30,
                TextColor = Colores.Textos,
                VerticalOptions = LayoutOptions.Center
            };

            LabelNombreEstudiante.SetBinding(Label.TextProperty, "Nombre_Completo");
            LabelMateria.SetBinding(Label.TextProperty, "Materia");
            LabelDocente.SetBinding(Label.TextProperty, "Docente");
            LabelNota.SetBinding(Label.TextProperty, "CalculoNota");

            Vista_Label.Children.Add(LabelNombreEstudiante);
            Vista_Label.Children.Add(LabelMateria);
            Vista_Label.Children.Add(LabelDocente);

            Celda_Principal.Children.Add(Vista_Label);
            Celda_Principal.Children.Add(LabelNota);

            View = Celda_Principal;
        }
    }
}