﻿using Android_1.Controlador;
using Android_1.Modelo;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
namespace Android_1.Vista
{
    public class Pagina_Principal : ContentPage
    {
        RelativeLayout Principal;
        Label Pagina;
        ListView listaDatosPersonales;
        List<Estudiante> DatosPersonales;
        BoxView Barra_Navegacion;
        Image Icono_Salir;
        TapGestureRecognizer Gesto_Salir;

        public Pagina_Principal()
        {
            
            CrearVistas();
            AgregarVista();
            AgregarEventos();
            this.BackgroundColor = Colores.Navegacion1;
        }

        void CrearVistas()
        {
            Gesto_Salir = new TapGestureRecognizer();
            Pagina = new Label
            {
                Text = "Pagina Principal",
                FontSize = 20,
                TextColor = Colores.Textos,
                HorizontalTextAlignment = TextAlignment.Center
            };

            Icono_Salir = new Image
            {
                Source = Colores.Icono_Salir
            };

            Barra_Navegacion = new BoxView
            {
                BackgroundColor = Colores.Navegacion
            };

            DatosPersonales = new List<Estudiante>();
            ListadoDeEstudiantes();

            listaDatosPersonales = new ListView
            {
                ItemsSource = DatosPersonales,
                ItemTemplate = new DataTemplate(typeof(CeldaEstudiantes)),
                RowHeight = 80,
                SeparatorColor = Colores.Textos
            };

            Principal = new RelativeLayout { BackgroundColor = Colores.Navegacion1 };
        }

        void AgregarVista()
        {
            Icono_Salir.GestureRecognizers.Add(Gesto_Salir);

            //Principal.Children.Add(Barra_Navegacion,
            //Constraint.RelativeToParent((p) => { return 0; }),                 //X : POSICION HORIZONTAL
            //Constraint.RelativeToParent((p) => { return 0; }),                 //Y : POSICION VERTICAL
            //Constraint.RelativeToParent((p) => { return p.Width; }),           //W : ANCHO DE CONTROL
            //Constraint.RelativeToParent((p) => { return p.Height * 0.083; })); //H : ALTO DE CONTROL

            //Principal.Children.Add(Pagina,
            //Constraint.RelativeToParent((p) => { return 0; }),                //X : POSICION HORIZONTAL
            //Constraint.RelativeToParent((p) => { return p.Height * 0.022; }), //Y : POSICION VERTICAL
            //Constraint.RelativeToParent((p) => { return p.Width; }));         //W : ANCHO DE CONTROL
            //Constraint.RelativeToParent((p) => { return p.Height * 0.083; }));//H : ALTO DE CONTROL

            //Principal.Children.Add(Icono_Salir,
            //Constraint.RelativeToParent((p) => { return p.Width  * 0.850; }),  //X : POSICION HORIZONTAL
            //Constraint.RelativeToParent((p) => { return 0; }),                 //Y : POSICION VERTICAL
            //Constraint.RelativeToParent((p) => { return p.Width  * 0.149 ; }), //W : ANCHO DE CONTROL
            //Constraint.RelativeToParent((p) => { return p.Height * 0.083; })); //H : ALTO DE CONTROL

            Principal.Children.Add(listaDatosPersonales,
            Constraint.RelativeToParent((p) => { return 0; }),                 //  X : POSICION HORIZONTAL
            Constraint.RelativeToParent((p) => { return p.Height * 0.070; }));  //  Y : POSICION VERTICAL
            //Constraint.RelativeToParent((p) => { return p.Height * 0.480; }));  //  W : ANCHO DE CONTROL
            //Constraint.RelativeToParent((p) => { return p.Height ; })); //  H : ALTO DE CONTROL

            Content = Principal;
        }

        void AgregarEventos()
        {
            Gesto_Salir.Tapped += Gesto_Salir_Tapped;
            listaDatosPersonales.ItemSelected += listaDatosPersonales_ItemSelected;
            listaDatosPersonales.ItemTapped += listaDatosPersonales_ItemTapped;
        }
        private async void listaDatosPersonales_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            await Navigation.PushAsync(new Detalle_Notas((Estudiante)e.Item));

        }

        private async void Gesto_Salir_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

        private void listaDatosPersonales_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }

        void ListadoDeEstudiantes()
        {
            DatosPersonales.Add(new Estudiante
            {
                Nombre = "Roney",
                Apellido = "Rodriguez",
                Docente = "Docente: Roney Rodriguez",
                Materia = "Electiva II",
                Nota1 = 3.5,
                Nota2 = 3.5,
                Nota3 = 3.5,

            });

            DatosPersonales.Add(new Estudiante
            {
                Nombre = "Roney",
                Apellido = "Rodriguez",
                Docente = "Docente: Roney Rodriguez",
                Materia = "POO II",
                Nota1 = 4.8,
                Nota2 = 4.8,
                Nota3 = 4.8,

            });

            DatosPersonales.Add(new Estudiante
            {
                Nombre = "Roney",
                Apellido = "Rodriguez",
                Docente = "Docente: Roney Rodriguez",
                Materia = "Web II",
                Nota1 = 5.0,
                Nota2 = 5.0,
                Nota3 = 5.0,
            });
        }
    }
}

