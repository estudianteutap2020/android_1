﻿using Android_1.Controlador;
using Android_1.Vista.Menu;
using System;
using Xamarin.Forms;
namespace Android_1.Vista
{
    public class MasterPage : MasterDetailPage
    {
        public MasterPage()
        {

            NavigationPage.SetHasNavigationBar(this, false);
            Page _Pagina_Principal = new VistaMenuMaster();
            Master = _Pagina_Principal;
            Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(Pagina_Principal)))
            {
                BarBackgroundColor = Colores.Navegacion,
                BarTextColor = Color.Black,
            };
        }
    }
}
