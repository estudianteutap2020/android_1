﻿using Android_1.Controlador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
namespace Android_1.Vista.Menu
{
    public class VistaMenuMaster : ContentPage
    {
        RelativeLayout Vista;
        Button Btn_Blanco, Btn_Titulo;
        ListView ListaMenu;
        List<MasterMenu> Menu;
        Image Nota, Nota1;

        public VistaMenuMaster()
        {
            Title = "MI Vista";
            CrearVistas();
            AgregarVista();
            AgregarEventos();

            void CrearVistas()
            {
                Nota = new Image
                {
                    Source = Colores.Nota
                };

                Nota1 = new Image
                {
                    Source = Colores.Nota1
                };

                Btn_Blanco = new Button
                {
                    BackgroundColor = Colores.Navegacion1,
                    IsEnabled = false
                };

                Btn_Titulo = new Button
                {
                    BackgroundColor = Colores.Navegacion1,
                    Text = "SOY UTAP",
                    FontSize = 30,
                    CornerRadius = 20,
                    BorderWidth = 2,
                    BorderColor = Colores.Botones,
                    TextColor = Colores.Titulos,
                };

                Menu = new List<MasterMenu>
                 {
                   new MasterMenu {Titulo="Mis materias y notas",Icono="Icon.png",IconoVisible=true},
                   new MasterMenu {Titulo="Mi documentación",Icono="Ic_add.png",IconoVisible=true},
                   new MasterMenu {Titulo="Cerrar Sesión",Icono="Cerrar_Sesion.png",IconoVisible=true},
                 };

                ListaMenu = new ListView
                {
                    ItemsSource = Menu,
                    ItemTemplate = new DataTemplate(typeof(EstiloTemplate)),
                    RowHeight = 70,
                    SeparatorColor = Colores.Textos1
                };

                Vista = new RelativeLayout { BackgroundColor = Colores.Navegacion };

            }

            void AgregarVista()
            {
                Vista.Children.Add(Btn_Blanco,
                Constraint.RelativeToParent((p) => { return 0; }),                  //X:Posición horizontal
                Constraint.RelativeToParent((p) => { return 0; }),                  //Y:Posición vertical
                Constraint.RelativeToParent((p) => { return p.Width * 0.999; }),    //W:Ancho de control
                Constraint.RelativeToParent((p) => { return p.Height * 0.250; }));  //H:Alto de control

                Vista.Children.Add(Btn_Titulo,
                Constraint.RelativeToParent((p) => { return p.Width * 0.070; }), 
                Constraint.RelativeToParent((p) => { return p.Height * 0.150; }),
                Constraint.RelativeToParent((p) => { return p.Width * 0.850; }), 
                Constraint.RelativeToParent((p) => { return p.Height * 0.200; }));

                Vista.Children.Add(Nota,
                Constraint.RelativeToParent((p) => { return p.Width * 0.070; }),  
                Constraint.RelativeToParent((p) => { return p.Height * 0.400; }), 
                Constraint.RelativeToParent((p) => { return p.Width * 0.150; }),  
                Constraint.RelativeToParent((p) => { return p.Height * 0.100; }));

                Vista.Children.Add(Nota1,
                Constraint.RelativeToParent((p) => { return p.Width * 0.070; }), 
                Constraint.RelativeToParent((p) => { return p.Height * 0.500; }),
                Constraint.RelativeToParent((p) => { return p.Width * 0.150; }), 
                Constraint.RelativeToParent((p) => { return p.Height * 0.100; }));

                Vista.Children.Add(ListaMenu,
                Constraint.RelativeToParent((p) => { return p.Width * 0.070; }), 
                Constraint.RelativeToParent((p) => { return p.Height * 0.400; }),
                Constraint.RelativeToParent((p) => { return p.Width * 0.850; }), 
                Constraint.RelativeToParent((p) => { return p.Height * 0.700; }));

                Content = Vista;
            }
        }
        void AgregarEventos()
        {
            ListaMenu.ItemSelected += ListaMenu_ItemSelected;
            ListaMenu.ItemTapped += ListaMenu_ItemTapped;
        }

        private void ListaMenu_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }

        private async void ListaMenu_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            switch (((MasterMenu)e.Item).Titulo)
            {
                case "Mis tareas y notas":
                    break;

                case "Mi documentacion":
                    break;

                case "Cerrar Sesion":
                    await App.Current.MainPage.Navigation.PushAsync(new Login());
                    Page pagina = App.Current.MainPage.Navigation.NavigationStack[0];
                    Navigation.RemovePage(pagina);
                    break;

                default:
                    break;
            }
        }
    }
}