﻿using Android_1.Controlador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Xamarin.Forms;
namespace Android_1.Vista
{
    public class Recuperar_Contraseña : ContentPage
    {
        RelativeLayout Principal;
        Label Pagina, Titulo;
        Entry Correo;
        Button Recuperar;
        BoxView Barra_Navegacion;
        Image Icono_Atras;
        TapGestureRecognizer Gesto_Atras;

        public Recuperar_Contraseña()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            CrearVista();
            AgregarVista();
            AgregarEvento();
            this.BackgroundColor = Colores.Navegacion1;
        }

        void CrearVista()
        {
            Gesto_Atras = new TapGestureRecognizer();

            Pagina = new Label
            {
                Text = "Pagina Principal",
                FontSize = 20,
                TextColor = Colores.Textos,
                HorizontalTextAlignment = TextAlignment.Center
            };

            Icono_Atras = new Image
            {
                Source = Colores.Icono_Atras
            };

            Barra_Navegacion = new BoxView
            {
                BackgroundColor = Colores.Navegacion
            };

            Titulo = new Label
            {
                TextColor = Colores.Titulos,
                Text = "Recupera tu contraseña",
                FontSize = 40,
                Margin = new Thickness(0, 0, 0, 30),
            };

            Correo = new Entry
            {
                Placeholder = "Correo institucional",
                PlaceholderColor = Colores.Textos1,
            };

            Recuperar = new Button
            {
                BackgroundColor = Colores.Navegacion1,
                Text = "Recuperar",
                CornerRadius = 25,
                BorderWidth = 2,
                BorderColor = Colores.Botones,
                TextColor = Colores.Titulos
            };

            Principal = new RelativeLayout { BackgroundColor = Colores.Navegacion1 };
        }

        void AgregarVista()
        {
            Icono_Atras.GestureRecognizers.Add(Gesto_Atras);

            Principal.Children.Add(Barra_Navegacion,
            Constraint.RelativeToParent((p) => { return 0; }),                 //X : Posición horizontal
            Constraint.RelativeToParent((p) => { return 0; }),                 //Y : Posición vertical
            Constraint.RelativeToParent((p) => { return p.Width; }),           //W : Ancho de control
            Constraint.RelativeToParent((p) => { return p.Height * 0.083; })); //H : Alto de control

            Principal.Children.Add(Pagina,
            Constraint.RelativeToParent((p) => { return 0; }),                 
            Constraint.RelativeToParent((p) => { return p.Height * 0.022; }),  
            Constraint.RelativeToParent((p) => { return p.Width; }));          
            //Constraint.RelativeToParent((p) => { return p.Height * 0.083; }));

            Principal.Children.Add(Icono_Atras,
            Constraint.RelativeToParent((p) => { return 0; }),                 
            Constraint.RelativeToParent((p) => { return 0; }),                 
            Constraint.RelativeToParent((p) => { return p.Width * 0.149; }),   
            Constraint.RelativeToParent((p) => { return p.Height * 0.083; })); 

            Principal.Children.Add(Titulo,
            Constraint.RelativeToParent((p) => { return p.Width * 0.205; }),  
            Constraint.RelativeToParent((p) => { return p.Height * 0.180; }), 
            Constraint.RelativeToParent((p) => { return p.Width * 0.793; })); 
            //Constraint.RelativeToParent((p) => { return p.Height *0.773; })); //H : ALTO DE CONTROL

            Principal.Children.Add(Correo,
            Constraint.RelativeToParent((p) => { return p.Width * 0.080; }),  
            Constraint.RelativeToParent((p) => { return p.Height * 0.400; }), 
            Constraint.RelativeToParent((p) => { return p.Width * 0.800; })); 
            //Constraint.RelativeToParent((p) => { return p.Height * 0.080; })); //H : ALTO DE CONTROL

            Principal.Children.Add(Recuperar,
            Constraint.RelativeToParent((p) => { return p.Width * 0.080; }),  
            Constraint.RelativeToParent((p) => { return p.Height * 0.500; }), 
            Constraint.RelativeToParent((p) => { return p.Height * 0.400; }));
             //Constraint.RelativeToParent((p) => { return p.Height * 0.080; })); //  H : ALTO DE CONTROL

            Content = Principal;
        }

        void AgregarEvento()
        {
            Gesto_Atras.Tapped += Gesto_Atras_Tapped;
            Recuperar.Clicked += Recuperar_Clicked;
        }

        private async void Recuperar_Clicked(object sender, EventArgs e)
        {
            Console.WriteLine(e);
            if (string.IsNullOrEmpty(Correo.Text))
            {
                await DisplayAlert("ERROR", "DEBE INGRESAR SU CORREO ELECTRONICO", "ACEPTAR");
                return;
            }

            if (Correo.Text.Contains("@") & Correo.Text.Contains(".com") || Correo.Text.Contains(".net") || Correo.Text.Contains(".edu.co"))
            {
                await DisplayAlert(" ", "POR FAVOR, REVISE SU CORREO ELECTRONICO", "ACEPTAR");
            }

            else
            {
                await DisplayAlert("ERROR", "DEBE INGRESAR SU CORREO CON @", "ACEPTAR");
                return;

            }
        }

        private async void Gesto_Atras_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }

    }
}
