﻿using Android_1.Vista;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
namespace Android_1.Modelo
{
    public class Estudiante : ContentPage
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Materia { get; set; }
        public string Docente { get; set; }
        public int Nota { get; set; }
        public string Nombre_Completo { get { return Nombre + " " + Apellido; } }
        public double Nota1 { get; set; }
        public double Nota2 { get; set; }
        public double Nota3 { get; set; }
        public double CalculoNota { get { return ((Nota1) * 0.3) + ((Nota2) * 0.3) + ((Nota3) * 0.4); } }

    }
}
