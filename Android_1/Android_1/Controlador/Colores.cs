﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
namespace Android_1.Controlador
{
    public static class Colores
    {
        public static Color Navegacion { get; } = Color.FromHex("FFF234");  // amarillo
        public static Color Navegacion1 { get; } = Color.FromHex("#FFFFFF");  // blanco
        public static Color Botones { get; } = Color.FromHex("#f9f3f1");     // blanco
        public static Color Botones1 { get; } = Color.FromHex("#f9f3f1");   // blanco
        public static Color Textos { get; } = Color.FromHex("#6dbaea");     // azul
        public static Color Textos1 { get; } = Color.FromHex("#546467");    // negrito raro
        public static Color Titulos { get; } = Color.FromHex("#04B404");    // negrito raro
        public static Color Resultado { get; } = Color.FromHex("#04B404");  // verde


        //Sección Imágenes
        public static ImageSource Icono_Salir { get; } = ImageSource.FromFile("Icono_Salir.png");
        public static ImageSource Icono_Atras { get; } = ImageSource.FromFile("Icono_Atras.png");
        public static ImageSource Logo { get; } = ImageSource.FromFile("Logo.png");
        public static ImageSource Icono_Carga { get; } = ImageSource.FromFile("Icono_Carga.png");
        public static ImageSource Nota { get; } = ImageSource.FromFile("Nota.png");
        public static ImageSource Nota1 { get; } = ImageSource.FromFile("Nota1.png");

    }
}